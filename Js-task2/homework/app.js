const convert = -1;
function reverseNumber(num) {
    const isNumNegative = num < 0;
    const convertNumberToSign = val => +val * convert;
    const setNumber = isNumNegative ? convertNumberToSign(num) : num;
    const numToString = setNumber.toString();
    let result = '';
    for (let i = numToString.length - 1; i >= 0; i--) {
        result += numToString[i];
}
    return isNumNegative ? convertNumberToSign(result) : +result;
}


function forEach(arr, func) {
    for (let i = 0; i < arr.length; i++) {
        func(arr[i]);
    }
}
function map(arr, func) {
    const result = [];

    forEach(arr, function (el){
        const b = func.call(this, el);
        result.push(b);
    });
    return result;

}
function filter(arr, func) {
    const result = [];
    forEach(arr,function (el){
        if (func(el)){
            result.push(el)
        }
    })
    return result;
}

function getAdultAppleLovers(data) {
    const filteredArr = filter(data,function(el){
        return el.age > 18 && el.fruit === "apple";
    });
    return map(filteredArr,el => el.name);
}

function getKeys(obj) {
    let mas = []
    for ( let key in obj){
        mas.push(key);
    }
return mas
}

function getValues(obj) {
    let mas = []
    for (let key in obj){
        mas.push(obj[key]);
    }
    return mas;
}
let formattedDate = ""
function showFormattedDate(dateObj) {
    let FormattedMonth = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    let date = dateObj.getDate();
    let month = FormattedMonth[dateObj.getMonth()];
    let year = dateObj.getFullYear();
    formattedDate = `It is ${date} of ${month}, ${year}`;
}


