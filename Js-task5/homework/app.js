fetch('./external-service.js')
    .catch(response => response.json())

const appRoot = document.getElementById('app-root');

let dataByRegion = externalService.getRegionsList();
let dataByLanguages = externalService.getLanguagesList();
let inputSearch = document.createElement('select');
let optionRegionsList = document.createElement('option');
let optionLanguagesList = document.createElement('option');

// create regionsListArray
let regionsListArray = [];

// create languagesListArray
let languagesListArray = [];

let allSelectedItemRegionsList;
let allSelectedItemByLanguages;

function createRadioElement() {
    let rootBlock = document.getElementById('app-root');

    let div = document.createElement('div');
    div.className = 'radioWrapper';

    // inpuByRegion
    let labelByRegion = document.createElement('label');
    labelByRegion.htmlFor = 'byRegion';
    let inputByRegion = document.createElement('input');
    inputByRegion.type = 'radio';
    inputByRegion.id = 'byRegion';
    inputByRegion.value = '1';
    inputByRegion.name = 'byRegion';
    inputByRegion.className = 'custom-checkbox';
    inputByRegion.checked = false;

    inputByRegion.onclick = function () {
        if(byRegion){
            // remove Old List
            document.getElementById('inputSearch').innerHTML='';

            //once more create default option
            let option = document.createElement('option');
            option.appendChild(document.createTextNode('Select value'));
            option.className = 'defaultValue';
            inputSearch.appendChild(option);

            // inputByLanguage unchecked
            inputByLanguage.checked = false;

            // create RegionsList  for select
            for (let i=0; i < externalService.getRegionsList().length; i++){
                optionRegionsList = document.createElement('option');
                optionRegionsList.appendChild(document.createTextNode(`${dataByRegion[i]}`));
                inputSearch.appendChild(optionRegionsList);
                regionsListArray.push(dataByRegion[i]);
            }

            let selectedItemRegionsList = document.getElementById('inputSearch');
            selectedItemRegionsList.onchange = function() {
                allSelectedItemRegionsList = externalService.getCountryListByRegion(selectedItemRegionsList.value);

                //generate_table
                // generate_table(allSelectedItemRegionsList);
            }
        }
    };

    labelByRegion.appendChild(inputByRegion);
    div.appendChild(labelByRegion);
    labelByRegion.appendChild(document.createTextNode('By Region'));

    //inputByLanguage
    let labelByLanguage = document.createElement('label');
    labelByLanguage.htmlFor = 'byLanguage';
    let inputByLanguage = document.createElement('input');
    inputByLanguage.type = 'radio';
    inputByLanguage.id = 'byLanguage';
    inputByLanguage.value = '2';
    inputByLanguage.name = 'byLanguage';
    inputByLanguage.className = 'custom-checkbox';

    inputByLanguage.onclick = function () {
        if(byLanguage){
            // remove Old List
            document.getElementById('inputSearch').innerHTML='';

            //once more create default option
            let option = document.createElement('option');
            option.appendChild(document.createTextNode('Select value'));
            option.className = 'defaultValue';
            inputSearch.appendChild(option);

            // inputByRegion unchecked
            inputByRegion.checked = false;

            // create LanguagesList for select
            for (let i=0; i < externalService.getLanguagesList().length; i++){
                optionLanguagesList = document.createElement('option');
                optionLanguagesList.appendChild(document.createTextNode(`${dataByLanguages[i]}`));
                inputSearch.appendChild(optionLanguagesList);
                languagesListArray.push(dataByLanguages[i]);
            }

            let selectedItemByLanguages = document.getElementById('inputSearch');
            selectedItemByLanguages.onchange = function() {
                allSelectedItemByLanguages = externalService.getCountryListByLanguage(selectedItemByLanguages.value);

                //generate_table
                // generate_table_by_Languages(allSelectedItemByLanguages);
                console.log(generate_table_by_Languages(allSelectedItemByLanguages),
                    'generate_table_by_Languages(allSelectedItemByLanguages);')
            }
        }
    };

    labelByLanguage.appendChild(inputByLanguage);
    div.appendChild(labelByLanguage);
    labelByLanguage.appendChild(document.createTextNode('By Language'));
    rootBlock.appendChild(div);
}
createRadioElement();

function createSearchElement() {
    let rootBlock = document.getElementById('app-root');
    let div = document.createElement('div');
    div.className = 'searchWrapper';

    // create description for Search input
    let labelBySearch = document.createElement('label');
    labelBySearch.appendChild(document.createTextNode('Please choose search query: '));

    // create input Search
    inputSearch.id = 'inputSearch';
    inputSearch.value = 'Select value';

    // add default option 'Select value'
    let option = document.createElement('option');
    option.appendChild(document.createTextNode('Select value'));
    option.className = 'defaultValue';
    inputSearch.appendChild(option);
    div.appendChild(inputSearch);
    rootBlock.appendChild(div);
    labelBySearch.appendChild(inputSearch);
    div.appendChild(labelBySearch);
}
createSearchElement();



// table for Languages
function generate_table_by_Languages(list) {
    let mainTables = document.getElementById('mainTable')
    if (document.getElementById('mainTable')){
        mainTables.parentNode.removeChild(mainTables);
    }
    let body = document.getElementsByTagName('body')[0];
    let table = document.createElement('table');
    table.id= 'mainTable';
    let tableBody = document.createElement('tbody');

    for (let i =0; i < list.length;i++){
        let rows = document.createElement('tr');

        for (key in list[i]){
            let cell = document.createElement('td');
            let cellText;

            if (key === 'flagURL'){
                let img = document.createElement('img');
                img.src = `${list[i][key]}`;
                cell.appendChild(img);
                cellText = document.createTextNode('')
            } else {
                if (key === 'languages'){
                    let languages = list[i][key];

                    for ( key in languages){
                        cellText = document.createTextNode(`${languages[key]}`)
                    }
                } else {
                    cellText = document.createTextNode(`${list[i][key]}`)
                }

            }
            cell.appendChild(cellText);
            rows.appendChild(cell)
        }
        tableBody.appendChild(rows)
    }
    table.appendChild(tableBody);
    body.appendChild(table);
    table.setAttribute('border','1');

}