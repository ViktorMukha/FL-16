function visitLink(path) {
let count = 1;
    if(localStorage.getItem(path)){
        count = +localStorage.getItem(path) + 1;
        localStorage.setItem(path,count);
    }else{
        localStorage.setItem(path,count)
    }
}
function viewResults() {
 let parents = document.querySelector('.btn-block');
 let ul = document.createElement('ul')
    for (let i = 0;i < localStorage.length;i++){
        let li = document.createElement('li');
        ul.appendChild(li);
         li.innerHTML = `You visited ${localStorage.key(i)} ${localStorage.getItem(localStorage.key(i))} time(s)`
    }
    parents.after(ul);
    localStorage.clear();
}
