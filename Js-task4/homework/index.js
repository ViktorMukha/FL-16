function year(yourDate){
    let todayDate = new Date();
    let yearDate = todayDate.getFullYear();
    let age = yearDate - yourDate;
    console.log(age);
}

function weekDay(date){
    let weekday=new Array(7);
    weekday[0]="Sunday";
    weekday[1]="Monday";
    weekday[2]="Tuesday";
    weekday[3]="Wednesday";
    weekday[4]="Thursday";
    weekday[5]="Friday";
    weekday[6]="Saturday";
    return weekday[date.getDay()]
}

function newYear(today){
   today=new Date();
    let cmas=new Date(today.getFullYear(), 11, 31);
    if (today.getMonth()===11 && today.getDate()>25){
        cmas.setFullYear(cmas.getFullYear()+1);
    }
    let oneDay=1000*60*60*24;
    return Math.ceil((cmas.getTime()-today.getTime())/oneDay)+ " days left until Christmas!";
}

function dayOfProgrammer(year){
    let date = new Date(year, 1, 29).getDate() === 29;
    if (date){
        return "12 september"
    } else {
        return '13 september'
    }
}

function howFar(day) {
    let specifiedWeekday = day;
    let today = new Date();
    let weekday = new Array(7);
    weekday[0] = "Sunday";
    weekday[1] = "Monday";
    weekday[2] = "Tuesday";
    weekday[3] = "Wednesday";
    weekday[4] = "Thursday";
    weekday[5] = "Friday";
    weekday[6] = "Saturday";
    let todayIs = weekday[today.getDay()];
    let numberDay;

    if(weekday.indexOf(specifiedWeekday) > weekday.indexOf(todayIs)) {
        numberDay = weekday.indexOf(specifiedWeekday) - weekday.indexOf(todayIs);
    }

    if(weekday.indexOf(todayIs) > weekday.indexOf(specifiedWeekday)){
        numberDay = weekday.indexOf('Saturday') - weekday.indexOf(todayIs) + weekday.indexOf(specifiedWeekday) + 1
    }

    if (todayIs === specifiedWeekday) {
        return `Hey, today is ${ todayIs } =)`;
    } else {
        return `It's ${numberDay} day(s) left till ${specifiedWeekday}.`;
    }
}

function isValidIdentifier(str) {
    return (/^[myVar]+[$|_]/g).test(str);
}

function capitalize(string){
    return string.replace(/\w\S*/g,
        function(txt){
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
}

function isValAudFile(string){
    return (/^[file]+(.mp3|.flac|.alac|.aac)/g).test(string);
}

function getHexColor(testOfString){
    return testOfString.match(/(?:#|0x)(?:[a-f0-9]{3}|[a-f0-9]{6})\b|(?:rgb|hsl)a?\([^)]*\)/ig);
}

function checkPassword(password){
    if (password.match(/^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9]=)(?=.*[a-z]).{8}$/)){
        return true;
    } else {
        return false;
    }
}

function urlify(text) {
    let urlRegex = /(https?:\/\/[^\s]+)/g;
    return text.replace(urlRegex, ' ["$1">$1]')
}
console.log(urlify('We use https://translate.google.com/ to translate some words and phrases from https://angular.io/'))