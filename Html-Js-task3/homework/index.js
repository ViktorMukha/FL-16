/* START TASK 1: Your code goes here */
window.onload = function (){
    const teds = document.getElementsByTagName('td');
    for (let i =0;i<teds.length;i++){
        teds[i].onclick = click;
    }
}
function click(){
    const parElement = this.parentElement;
    const ells = parElement.querySelectorAll('td');

    if (parElement.firstElementChild === this){
        if (!parElement.querySelector('.yellow')){
            ells.forEach(function (elBlue){
                elBlue.classList.add('blue')
            })
        } else {
            this.classList.add('blue')
        }
    } else {
        this.classList.add('yellow')
    }

    const specialCell = document.querySelector('.specialCell');
    const allBlocks = document.querySelectorAll('td');
    const allBlocksArrays = [...allBlocks];

    if (specialCell === this){
        allBlocksArrays.forEach(td => {
            if (!td.classList.contains('blue') || !td.classList.contains('yellow')){
                td.classList.add('yellow')
            }
        })
    }
}
/* END TASK 1 */

/* START TASK 2: Your code goes here */
function phoneCheck (){
    const inputText = document.getElementById("phoneNumber");
    const regButton = document.getElementById('submit');
    const inputStripped = inputText.value.replace(/\D/g,'');
    const res = document.getElementById("massage");
    const phoneReg = /^[0-9()-.\s]+$/
    res.className = "";
    if (phoneReg.test(inputText.value) && inputStripped.length >= 10) {
        res.className = "yup";
        res.innerHTML= "Data successful";
        res.classList.add('successful')
    } else {
        res.className = "nope";
        res.innerHTML= "Type phone number in format +380*********";
        res.classList.add('invalid')
        regButton.disabled = true;
        console.log(this);
        inputText.value = ''
    }
}
phoneCheck()
/* END TASK 2 */
