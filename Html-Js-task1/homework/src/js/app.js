// Your code goes here
let meeting = prompt("Need name","meeting");


document.getElementById('confirm').addEventListener('click',function (event){
    event.preventDefault();
   condition(event)
})
function condition (event) {
    let name = document.getElementById('name').value;
    let time = document.getElementById('time').value;
    let place = document.getElementById('place').value;
    if (name === "" || time === "" || place === "") {
        alert('Input all data');
        return false;
    } else {
        validateTime(name,time,place,event)
    }
}

function validateTime(name,time,place,event){
    event.preventDefault();
    let timeTest = /^(2[0-3]|[01]?[0-9]):([0-5]?[0-9])$/;
    let timeRes = timeTest.test(time)
    if (!timeRes){
        alert('Need time format HH:MM')
    } else {
        alert(`${name} has a ${meeting} today at ${time} in ${place}`)
    }
}

document.getElementById("converter").addEventListener('click',function (event){
    event.preventDefault()
    let convertEuro = 30;
    let convertDollar = 28;

    let euro = function ask(){
        let euroPrompt = prompt("Enter sum dollar,more then 0",50);
        return isNaN(euroPrompt) || +euroPrompt < 1 ? ask() : euroPrompt
    };
    let dollar = function ask(){
        let dollarPrompt = prompt("Enter sum dollar,more then 0",150);
        return isNaN(dollarPrompt) || +dollarPrompt < 1 ? ask() : dollarPrompt;
    }

    let finEuro =`${euro} euros are equal ${(euro * convertEuro).toFixed(2)}hrn`;
    let finDollar =`${dollar} dollars are equal ${(dollar * convertDollar).toFixed(2)}hrn`;
    alert(`${finEuro},${finDollar}`);
})
