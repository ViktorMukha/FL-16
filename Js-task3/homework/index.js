// Your code goes here
let isEquals = (a,b) => a === b;

let isBigger = (a,b) => a > b;

function storeNames(...names){
    let mas = names;
    return mas;
}

function getDifference(first,second){
    if (first > second){
        return first - second;
    } else {
        return second - first;
    }
}

function negativeCount (array) {
    let arrMinus = [];
    for (let i = 0;i<array.length;i++){
        if (array[i] < 0){
            arrMinus.push(array[i])
        }
    }

    return arrMinus.length;
}

function letterCount(word,letter) {
    return word.toLowerCase('').split('').filter(el => el === letter).length;
}

function countPoints(games) {
    let points = 0;

    for (let i = 0; i < games.length; i++) {

        let goals = games[i].split(':');

        if (+goals[0] > +goals[1]) {
            points += 3;
        } else if (+goals[0] === +goals[1]) {
            points += 1;
        }
    }
    return points;
}
