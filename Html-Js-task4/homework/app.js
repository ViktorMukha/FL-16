const root = document.getElementById('root');
window.onload = function (){
    const teds = document.getElementsByTagName('td');
    for (let i =0;i<teds.length;i++){
        teds[i].onclick = click;
    }
}
function click(){
    const parElement = this.parentElement;
    const ells = parElement.querySelectorAll('td');

    if (parElement.firstElementChild === this){
        if (!parElement.querySelector('.yellow')){
            ells.forEach(function (elBlue){
                elBlue.classList.add('blue')
            })
        } else {
            this.classList.add('blue')
        }
    } else {
        this.classList.add('yellow')
    }

    const specialCell = document.querySelector('.specialCell');
    const allBlocks = document.querySelectorAll('td');
    const allBlocksArrays = [...allBlocks];

    if (specialCell === this){
        allBlocksArrays.forEach(td => {
            if (!td.classList.contains('blue') || !td.classList.contains('yellow')){
                td.classList.add('yellow')
            }
        })
    }
}